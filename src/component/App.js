import React, { Component } from 'react'
import { Header } from './Header'
import HomePage from './pages/HomePage'
import FashionPage from './pages/FashionPage'
import { Coupons } from './profile/Coupons'
import { CouponExpire } from './profile/Coupon expire'
import Points from './profile/Points'
import Portefeuilles from './profile/Portefeuilles'
import Cadeau from './profile/Cadeau'
import { Login } from '../containers/Login'
import { Signup } from '../containers/Signup'
import { Profile } from '../containers/Profile'
import { Logout } from '../containers/Logout'
import Footer from './profile/Footer'
import {
  Switch,
  Route,
  Redirect,
  useParams,
  HashRouter,
} from 'react-router-dom'
import Collapse from '../containers/Collapse'



function PrivateRoute ({ children, ...rest }) {
  let auth = localStorage.getItem('user')
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  )
}

function LogementPageFunc (props) {
  let { id } = useParams()

  let fashion = props.fashions.find(x => x.id === id)
  if (fashion !== undefined) {
    return <FashionPage fashion={fashion}/>
  } else {
    return <Redirect to="/error"/>
  }
}

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      fashions: this.props.fashions,
    }
  }

  render () {
    return (
      <HashRouter>
        <Header/>


        <Switch>

          <Route exact path="/">
            <HomePage fashions={this.state.fashions}/>

          </Route>

          <PrivateRoute path="/user">
            <Profile fashions={this.state.fashions}/>
            <Collapse/>
          </PrivateRoute>
          <Route path="/login" component={Login}/>
          <Route path="/logout" component={Logout}/>
          <Route path="/signup" component={Signup}/>
          <Route path="/coupon">
            <Coupons/>

            <CouponExpire/>
            <Footer/>
          </Route>
          <Route path="/point">
            <Points/>

          </Route>
          <Route path="/portefeuille">
            <Portefeuilles/>
          </Route>
          <Route path="/cadeau">
            <Cadeau/>
          </Route>

          <Route path="/fashion/:id">
            <LogementPageFunc fashions={this.state.fashions}/>

          </Route>

        </Switch>


      </HashRouter>
    )
  }
}

export default App
