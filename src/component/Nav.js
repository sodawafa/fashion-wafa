import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import '../styles/coupon.css'
function getClass (currentPathname, pathname) {
  if (currentPathname === pathname) return 'active'
  return ''
}
function Nav () {
  const location = useLocation()

  return (<nav>
      <ul className={'list'}>
        <li>
          <Link to="/coupon non utilise"  id="main-title" className={getClass(location.pathname, '/')}>Coupons non
            utilisés</Link>
        </li>
        <li>
          <Link to="/coupon-expires" id="main-title" className={getClass(location.pathname, '/coupon-expires')}>Coupons expirés</Link>
        </li>
      </ul>
    </nav>
  )


}
export default Nav
