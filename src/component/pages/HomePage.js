import React, { Component } from 'react'
import Banner from '../Banner'
import Clothing from '../Clothing'
import Footer from '../profile/Footer'

class HomePage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      fashions: this.props.fashions,
    }
  }

  render () {
    return (
      <div>
        <Banner/>
        <div className={'clothings'}>

          {
            this.state.fashions && this.state.fashions.map(fashion => {
              return (
                <Clothing name={fashion.title}
                          picture={fashion.picture}
                          key={'clothings_' + fashion.id.toString()}
                          id={fashion.id}

                />
                )
            })

          }

        </div>
        <footer>
          <Footer/>
        </footer>
      </div>
    )
  }

}

export default HomePage
