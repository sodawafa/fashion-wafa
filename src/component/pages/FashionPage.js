import React, { Component } from 'react'
import Name from '../Name'
import Picture from '../Picture'
import Ratting from '../Ratting'
import Price from '../Price'
import Footer from '../profile/Footer'
import '../../styles/fashion.css'

class FashionPage extends Component {

  constructor (props) {
    super(props)
    this.state = {
      fashion: this.props.fashion,
    }
  }

  render () {
    return (
      <div className={'container'}>
        <div className={'image'}>
          {

            this.state.fashion && this.state.fashion.pictures.map(
              (name, index) => {
                return <div>
                  <Picture src={name} key={'pictures_' + index}/>
                  <Name name={this.state.fashion.names[index]}
                        key={'names_' + index}/>
                  <Price name={this.state.fashion.prices[index]}
                         key={'prices_' + index}/>
                  <Ratting max={5} rate={this.state.fashion.rattings[index]}
                           key={'rattings_' + index}/>


                </div>
              })
          }
          <footer>
            <Footer/>
          </footer>
        </div>



      </div>

    )
  }
}

export default FashionPage

