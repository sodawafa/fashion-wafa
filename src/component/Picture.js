import React, { Component } from 'react'
import '../styles/picture.css'

class Picture extends Component {

  constructor (props) {
    super(props)
    this.state = {
      src: this.props.src,
    }
    console.log('kkk', this.props.src)

  }


  render () {
    return (
      <div className={'picture'}>
        <img src={this.state.src} alt={'picture_'}/>
      </div>
    )
  }
}

export default Picture
