import React from'react'
import '../styles/clothing.css'
import { Link } from 'react-router-dom'
import Picture from './Picture'

class Clothing extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      id: this.props.id,
      cover: this.props.cover,
      picture: this.props.picture,
      name: this.props.name,
    }
    console.log('ddddd', this.props.cover)
  }
render () {
    return(
      <Link to={'/fashion/' + this.state.id}
            className={"clothing"}
            key={'clothing_' + this.state.id}>
        <div>
          <div>
            <span className={'clothing-title'}>{this.state.name} </span> <Picture src={this.state.picture}/>
          </div>
          <p>{this.state.price}</p>
          <p>{this.state.cover}</p>
        </div>


      </Link>


    )
}
}
export default Clothing
