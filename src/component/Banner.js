import React, { Component } from 'react'
import '../styles/banner.css'
class Banner extends Component {
  render () {
    return(
<div className={"banner"}>
  <section className={'banner-content'}>
    <h1 className={"title"}>WAFA SODA</h1>
    <p className={"subtitle"}>fashion, Style & Look</p>
  </section>

</div>

    )
  }
}
export default Banner
