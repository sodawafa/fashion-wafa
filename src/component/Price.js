import React, { Component } from 'react'

class Price extends Component {

  constructor (props) {
    super(props)
    this.state = {
      name: this.props.name
    }
  }
  render () {
    return(
      <div>
        <span>
          {this.state.name}
        </span>
      </div>
    )
  }
}
export default Price
