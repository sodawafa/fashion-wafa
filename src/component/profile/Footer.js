import React from 'react'
import { Link } from 'react-router-dom'
import '../../styles/footer.css'
import apple from '../../assets/icone apple.png'
import android from '../../assets/icone androide.png'
class Footer extends React.Component {
  constructor (props) {
    super(props)

  }

  render () {
    return (
      <footer className={'footer-container'}>
        <div className={'footer-head'}>
          <div className={'footer-row'}>
            <div className={'container'}>
              <div className={'row-left'}>
                <div className={'footer-row'}>
                  <div className={'row-list-left'}>
                    <h6>Informations sur L'entreprise</h6>
                    <ul>
                      <li>
                        <Link to={'#'}>Qui sommes-Nous?</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Affilié</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Blogger</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Responsabilité Sociétale et
                          environnementale</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Responsabilité sociale</Link>
                      </li>
                    </ul>

                  </div>
                  <div className={'row-list-left'}>
                    <h6>Aide</h6>
                    <ul>
                      <li>
                        <Link to={'#'}>Livraison</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Retour</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Commande</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Status de Commande</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Guide de Tailles</Link>
                      </li>
                    </ul>

                  </div>
                  <div className={'row-list-left'}>
                    <h6>Contact & Paiement</h6>
                    <ul>
                      <li>
                        <Link to={'#'}>Nous Contacter</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Paiement</Link>
                      </li>
                      <li>
                        <Link to={'#'}>Point Bonus</Link>
                      </li>
                    </ul>

                  </div>

                </div>

              </div>
              <div className={'row-right'}>
                <div className={'row'}>
                  <div className={'row-chat'}>
                    <h6>Trouvez-nous sur</h6>
                    <ul className={'list-right'}>
                      <li>
                        <Link to={'#'}>
                          <img
                            src="//img.ltwebstatic.com/images3_pi/2019/10/16/15711951926b9f9b12026b560219c0b20efdbc053a.png"
                            alt=""/>
                        </Link>
                      </li>
                      <li>
                        <Link to={'#'}>
                          <img
                            src="//img.ltwebstatic.com/images3_pi/2019/10/16/1571195196822e2c36ccd1a522414b69f9985886b6.png"
                            alt=""/>

                        </Link>
                      </li>
                      <li>
                        <Link to={'#'}>
                          <img
                            src="//img.ltwebstatic.com/images3_pi/2019/10/16/1571195224e9c8e8091fcac78764a2f47656e85993.png"
                            alt=""/>

                        </Link>
                      </li>
                      <li>
                        <Link to={'#'}>
                          <img
                            src="//img.ltwebstatic.com/images3_pi/2019/10/16/15711952516cae745884339a9aa2d5f1b18052c433.png"
                            alt=""/>

                        </Link>
                      </li>
                      <li>
                        <Link to={'#'}>
                          <img
                            src="//img.ltwebstatic.com/images3_pi/2019/10/16/1571195264e7707098cef2d15eb0d55fd4b02bb0fd.png"
                            alt=""/>

                        </Link>
                      </li>
                      <li>
                        <Link to={'#'}>
                          <img
                            src="//img.ltwebstatic.com/images3_pi/2019/10/16/1571195285a71eb9c76e7d0d4179ac8f219452c0c4.png"
                            alt=""/>

                        </Link>
                      </li>
                      <li>
                        <Link to={'#'}>
                          <img
                            src="//img.ltwebstatic.com/images3_acp/2020/05/04/15885854601108095657ed6b547d1ec2b8a80cc866.png"
                            alt=""/>

                        </Link>
                      </li>
                    </ul>

                  </div>
                  <div className={'row-mobile'}>
                    <h6>APP</h6>
                    <ul>
                       <li>
                         <Link to={"#"}>
                           <img src={apple} alt="" style={{width: 35 , backgroundRepeat:'no-repeat',paddingRight:20}}/>
                         </Link>
                       </li>
                      <li>
                        <Link to={"#"}>
                          <img src={android} alt="" style={{width: 35 , backgroundRepeat:'no-repeat',paddingRight:20 }}/>

                        </Link>
                      </li>
                    </ul>
                  </div>

                </div>
                <div className={'row-email'}>
                  <h6>
                    Abonnez-vous à notre newsletter pour suivre toute l'actualité FASHION-WAFA en avant-première !
                    <br/>
                    (Vous pouvez vous DÉSABONNER à tout moment)</h6>
                  <div className={'row-subscribe'}>
                    <div className={'row-subscribe-input'}>
                      <input type="email" placeholder={"Votre Adresse E-mail"}/>

                    </div>
                    <div className={'row-subscribe-btn'}>
                      <button className={'subscribe'} type={'submit'}>S'abonner</button>

                    </div>

                  </div>

                </div>
                <div className={'row-payment'}>
                  <h6>NOUS ACCEPTONS</h6>
                  <div className={'bank-img'}>
                    <img src="//img.ltwebstatic.com/images2_pi/2018/06/06/15282730981571339584.png" alt=""/>
                    <img src="//img.ltwebstatic.com/images3_pi/2021/03/09/161528368123dd7a35ad8708b0dfc74b3630526891.png" alt=""/>
                    <img src="//img.ltwebstatic.com/images2_pi/2018/06/06/15282732803587566708.png" alt=""/>
                    <img src="//img.ltwebstatic.com/images2_pi/2018/06/06/15282732983375743706.png" alt=""/>
                    <img src="//img.ltwebstatic.com/images2_pi/2018/06/06/1528273036537082707.png" alt=""/>
                    <img src="//img.ltwebstatic.com/images2_pi/2018/06/06/15282733431754785346.png" alt=""/>

                  </div>

                </div>

              </div>
            </div>


          </div>
          <div className={'footer-row'}>
            <div className={'row-bottom'}>
              <span>
                  ©2009-2021 SHEIN Tous droits réservés
                </span>
              <ul className={'row-bottom-list'}>
                <li>
                  <Link to={"#"}>
                    Centre de confidentialité

                  </Link>
                </li>
                <li>
                  <Link to={'#'}>
                    Politique de confidentialité & cookies

                  </Link>
                </li>
                <li>
                  <Link to={"#"}>
                    Gérer les cookies

                  </Link>
                </li>
                <li>
                  <Link to={"#"}>
                    Termes et conditions

                  </Link>
                </li>
                <li>
                  <Link to={"#"}>
                    Notice de droits d'auteur

                  </Link>
                </li>   <li>
                  <Link to={"#"}>
                    Imprint

                  </Link>
                </li>

              </ul>

            </div>
            <div className={'row-bottom-payment'}>
              <img src="//sheinsz.ltwebstatic.com/she_dist/images/seal-43f93aaef6.png" alt=""/>
              <img src="//sheinsz.ltwebstatic.com/she_dist/images/DMCA-53fc2e1a50.png" alt=""/>

            </div>

          </div>

        </div>

      </footer>
    )
  }

}

export default Footer
