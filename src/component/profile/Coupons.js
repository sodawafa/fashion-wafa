import React from 'react'
import '../../styles/coupon.css'
import { connect } from 'react-redux'
import Footer from './Footer'

/**
 * @param mydate:string = "23/06/2021"
 */
function verifDate (mydate) {
  let date = parseDate(mydate)
  let today = new Date()
  date.setHours(0, 0, 0)
  today.setHours(0, 0, 0)
  console.info(date)
  console.info(today)
  console.info(compareDate(date, today))
}

/**
 * @param mydate:string = "23/06/2021"
 */
const parseDate = (mydate) => {
  const [day, month, year] = mydate.split('/')
  return new Date([month, day, year].join('/'))
}

function compareDate (date, today) {
  date.getDay()
  date.getMonth()
  date.getFullYear()
  today.getFullYear()
  today.getMonth()
  today.getDay()
  if (date.getFullYear() > today.getFullYear()) {
    return 'valider'
  } else if (date.getFullYear() < today.getFullYear()) {
    return 'expiré'
  } else {
    if (date.getMonth() > today.getMonth()) {
      return 'valider'
    } else if (date.getMonth() < today.getMonth()) {
      return 'expiré'
    } else {
      if (date.getDay() > today.getDay()) {
        return 'valider'
      } else if (date.getDay() < today.getDay()) {
        return 'expiré'
      }
    }
  }
  return 0

}

class Coupons extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    verifDate('28/09/2021')

  }

  render () {
    let couponExpire = false

    return (
      <div>
        <header>
          <h1 className={'title'}>MES COUPONS</h1>
        </header>
        <main>
          <div className={'nav-header'}>
            <div role={'tablist'} className={'nav-body'}>
              <div className={'nav-title1'}>

                <span tabIndex={'0'} role={'tab'} aria-label={'tab-item'}>
                  Coupons non utilisés
                </span>
              </div>
            </div>
            <div className={'nav-title2'}>

              <span tabIndex={'0'} role={'tab'} aria-label={'tab-item'}>Coupons expirés</span>
            </div>
          </div>
          <section className={'content-item'}>
            <div>
              <ul className={'content-list'}>
                {this.props.coupons && this.props.coupons.map((coupon, key) => {
                  console.log( !couponExpire && verifDate >
                    compareDate)
                  if (!couponExpire && verifDate >
                    compareDate) {
                    return (
                      <li className={'main-content'} key={key}>
                        <div>
                          <div className={'head'}>
                            <div className={'coupon-title'}>
                            <span
                              className={'prix'}>{coupon.discount}</span>
                            </div>
                            <p>{coupon.validity}</p>
                            <p className={'coupon-item-content'}>
                              <span>Code: {coupon.coded}</span>
                            </p>

                          </div>
                          <div className={'line'}>

                          </div>
                          <div className={'body'}>
                            <li className={'data-rang'}>
                              <span>{coupon.date}</span>
                            </li>
                            <li className={'data-rang'}>
                              <span>Sur tous les produits</span>

                            </li>

                          </div>

                        </div>
                      </li>
                    )
                  }else {
                    if (couponExpire && compareDate < verifDate){
                      console.log(couponExpire && compareDate < verifDate)
                      return false
                    }
                  }
                  }

                )

                }
                  </ul>
                  </div>

                  </section>

                  </main>

        <footer>
           <Footer/>
        </footer>
                  </div>


                  )
                  }
                  }

                  const mapStateToProps = state => ({
                  coupons: state.authentication.coupons,
                })

                  const couponsPage = connect(mapStateToProps)(Coupons)
                  export {couponsPage as Coupons}


