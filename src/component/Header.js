import React, { Component } from 'react'
import '../styles/header.css'
import logoImg from '../assets/logo-wafa.jpg'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class Header extends Component {
  render () {
    return (
      <header className={'titre-content'}>
        <img src={logoImg} alt="fashion wafa"/>
        <nav>
          <ul >
            <li>
              <Link to="/">Accueil</Link>
            </li>
            {
              (!this.props.user) ?
                (
                  <ul>
                  <li>
                  {/*<Link to="/signup" >Signup</Link>*/}
                  <Link to="/login" >Login</Link>
                  <Link to="/signup" >Signup</Link>
                </li>
                  </ul>
               ) :
                (
                  <li>
                    <ul>
                      <li>
                        <Link className="main-nav-item" to="/user">
                          {this.props.user.firstName}
                        </Link>
                      </li>
                      <li>
                        <Link className="main-nav-item" to="/logout">
                          Sign Out
                        </Link>
                      </li>
                    </ul>
                  </li>
                )

            }
       {/*     {
              (!this.props.user) ?
                (
                  <ul>
                    <li>
                      <Link to="/signup" >Signup</Link>

                    </li>
                  </ul>

                ) :
                (
                  <li>
                    <ul>
                      <li>
                        <Link className="main-nav-item" to="/">
                          {this.props.user.firstName}
                        </Link>
                      </li>
                      <li>
                        <Link className="main-nav-item" to="/logout">
                          Sign Out
                        </Link>
                      </li>
                    </ul>
                  </li>
                )

            }*/}


          </ul>
        </nav>
      </header>
    )
  }

}

const mapStateToProps = state => ({
  user: state.authentication.user,
})

const connectedLoginPage = connect(mapStateToProps)(Header)
export { connectedLoginPage as Header }
