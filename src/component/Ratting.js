import React, { Component } from 'react'
import star0 from '../assets/star0.svg'
import star1 from '../assets/star1.svg'
import '../styles/ratting.css'

class Ratting extends Component {

  constructor (props) {
    super(props)
    this.state = {
      max: this.props.max,
      rate: this.props.rate
    }
  }
  getStart() {
    let html = []
    if (this.state.max >= this.state.rate){
      for (let i = 0 ; i < this.state.max ; i++){
        if (i < this.state.rate){
          html.push(<img src={star1} alt={'star_1'} key={'star_ ' + i}/>)
        }
        else {
          html.push(<img src={star0} alt={'star_0'} key={'star_ ' + i}/>)
        }

      }
    }
    return html
  }

  render () {
    return (
      <div className={'stars'}>
        {this.getStart() && this.getStart().map(s => {
          return s
        })}
      </div>
    )
  }
}

export default Ratting
