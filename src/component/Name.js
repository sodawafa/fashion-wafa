import React, { Component } from 'react'

class Name extends Component {

  constructor (props) {
    super(props)
    this.state = {
      name: this.props.name
    }
  }
  render () {
    return (
      <div className={'name'}>
        <span>
          {
            this.state.name
          }
        </span>


      </div>
    )
  }

}
export default Name
