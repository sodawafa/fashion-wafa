import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import App from './component/App';
import reportWebVitals from './reportWebVitals'
import api, { URL } from './api'
import store from './redux/store'
import '../node_modules/font-awesome/css/font-awesome.min.css';
api(URL).then(
  json => {
      ReactDOM.render(
        <React.StrictMode>
          <Provider store={store}>
            <App fashions={json}/>
          </Provider>
        </React.StrictMode>,
        document.getElementById('root')
    )
  },
)

reportWebVitals()





/*
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './component/App';
import reportWebVitals from './reportWebVitals';
import api, { URL } from './api'
import store from './redux/store'
import {
  BrowserRouter as Router,
} from 'react-router-dom'
import { Provider } from 'react-redux'
api(URL).then(
  json => {
    ReactDOM.render(
      <React.StrictMode>
        <App fashions={json}/>
      </React.StrictMode>,
      document.getElementById('root'),
    )
  },
).catch(e => {

ReactDOM.render(
  <React.StrictMode>
<Provider store={store}>
  <App/>
</Provider>
  </React.StrictMode>,
  document.getElementById('root')
)
})

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
*/
