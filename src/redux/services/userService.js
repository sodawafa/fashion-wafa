import { HEADERS, API_URL } from '../constants/configApi'

export const userService = {
  login,
  signup,
  logout,
  profile,
  isAuthenticated,
  updateProfile,
}

function login (username, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email: username, password: password }),
    /*body: JSON.stringify({ email: "tony@stark.com", password: "password123" }),*/
  }

  return fetch(`${API_URL}/user/login`, requestOptions)
    .then(handleResponse)
    .then(response => {
        if (response.status === 200) {
          const user = {
            username: username,
            token: response.body.token ? response.body.token : '',
          }
          return this.profile(user).then(
            userInfo => {
              const myUser = {
                ...user,
                ...userInfo,
              }
              localStorage.setItem('user', JSON.stringify(myUser))
              return myUser
            },
            error => Promise.reject(error),
          )
        }
      },
      error => Promise.reject(error),
    )

}

function signup (firstName, lastName, email, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      email: email,
      password: password,
      firstName: firstName,
      lastName: lastName,
    }),
  }
  return fetch(`${API_URL}/user/signup`, requestOptions)
    .then(handleResponse)
    .then(
      response => {
        if (response.status === 200) {
          const email = response.body.email
          console.log(email)
          return email
        }
      }
    )
}

function profile (user = null) {
  if (!user) return null
  const requestOptions = {
    method: 'POST',
    headers: HEADERS(user.token),
  }
  return fetch(`${API_URL}/user/profile`, requestOptions)
    .then(handleResponse)
    .then(
      response => {
        const userInfo = response.body
        localStorage.setItem('user', JSON.stringify({
          ...user,
          ...userInfo,
        }))
        return response.body
      },
    )

}

function updateProfile (user, firstName, lastName) {
  const requestOptions = {
    method: 'PUT',
    headers: HEADERS(user.token),
    body: JSON.stringify({ firstName: firstName, lastName: lastName }),
  }
  return fetch(`${API_URL}/user/profile`, requestOptions)
    .then(handleResponse)
    .then(
      response => {
        if (response.status === 200) {
          const userInfo = response.body
          const myUser = {
            ...user,
            ...userInfo,
          }
          localStorage.setItem('user', JSON.stringify(myUser))
          return myUser
        }
      },
      error => Promise.reject(error),
    )

}

function isAuthenticated () {
  const json = localStorage.getItem('user')
  const user = json ? JSON.parse(json) : null
  return (user && user.token) ? user : null

}

function logout () {
  localStorage.removeItem('user')

}

function handleResponse (response) {
  return response.text().then(
    text => {
      const data = JSON.parse(text)
      if (!response.ok) {
        if (response.status === 401) {
          logout()
          window.location.reload(true)
        }
        const error = (data && data.message) || data.statusText
        return Promise.reject(error)
      }
      return data
    },
  )

}
/*function writeDateCookie (date) {
  document.cookie = `savedDate=${date.toString()}`
}
function getDateCookie() {

  const cookieObj = Object.fromEntries(
    document.cookie.split("; ").map(c => {
      const [key, ...v] = c.split("=");
      return [key, v.join("=")];
    })
  );
  return cookieObj.savedDate || null;

}*/
