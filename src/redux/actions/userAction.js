import { ACTIONS_TYPES } from '../constants'
import { userService } from '../services'
import store from '../store'

export const userAction = {
  login,
  signup,
  logout,
  profile,
  isAuthenticated,
  updateProfile,
  getCoupons,
}

function isAuthenticated () {
  return dispatch => {
    let user = store.getState().authentication.user
    user = user ? user : userService.isAuthenticated()
    user && dispatch(success(user))
    return user
  }

  function success (user) {
    return {
      type: ACTIONS_TYPES.USER_AUTHENTICATED,
      user,
    }

  }

}

function login (props, username, password) {
  return dispatch => {
    dispatch(request({ username }))
    userService.logout()
    userService.login(username, password).then(
      user => {
        dispatch(success(user))
        props.history.push('/user')
      },
      error => {
        dispatch(failure(error))
      },
    )
  }

  function request (user) {
    return {
      type: ACTIONS_TYPES.LOGIN_REQUEST,
      user,
    }

  }

  function success (user) {
    return {
      type: ACTIONS_TYPES.LOGIN_SUCCESS,
      user,
    }

  }

  function failure (error) {
    return {
      type: ACTIONS_TYPES.LOGIN_FAILURE,
      error,
    }

  }

}

function signup (props, email, password, firstName, lastName,) {
  return dispatch => {
    dispatch(request(email))
    userService.logout()
    userService.signup(firstName, lastName, email, password).then(
      email => {
        dispatch(success(email))
        props.history.push('/login')
      },
      error => {
        dispatch(failure(error))
      },
    )

  }

  function request (user) {
    return {
      type: ACTIONS_TYPES.SIGNUP_REQUEST,
      user,
    }

  }

  function success (user) {
    return {
      type: ACTIONS_TYPES.SIGNUP_SUCCESS,
      user,
    }

  }

  function failure (error) {
    console.log(error)
    return {
      type: ACTIONS_TYPES.SIGNUP_FAILURE,
      error,
    }

  }

}

function logout (props) {
  userService.logout()
  props.history.push('/')
  return {
    type: ACTIONS_TYPES.LOGOUT,
  }

}

function profile (user) {
  return dispatch => {
    dispatch(request())
    userService.profile(user).then(
      user => dispatch(success(user)),
      error => dispatch(failure(error)),
    )
  }

  function request (user) {
    return {
      type: ACTIONS_TYPES.PROFILE_REQUEST,
      user,
    }

  }

  function success (user) {
    return {
      type: ACTIONS_TYPES.PROFILE_SUCCESS,
      user,
    }

  }

  function failure (error) {
    return {
      type: ACTIONS_TYPES.PROFILE_FAILURE,
      error,
    }

  }

}

function getCoupons () {
  return dispatch => {
    dispatch({
      type: ACTIONS_TYPES.COUPONS_GET,
    })
  }

}

/*function setCoupons () {
  console.log(setCoupons(),"cccc")
  return dispatch => {
    dispatch(userService.getDateCookie())
    return {
      type: ACTIONS_TYPES.COUPON_EXPIRED_SET,
    }
  }

}*/

function updateProfile (user, firstName, lastName) {
  return dispatch => {
    dispatch(request(user))
    userService.updateProfile(user, firstName, lastName).then(
      user => {
        dispatch(success(user))
      },
      error => {
        dispatch(failure(error))
      },
    )
  }

  function request () {
    return {
      type: ACTIONS_TYPES.PROFILE_REQUEST,
    }

  }

  function success (user) {
    return {
      type: ACTIONS_TYPES.PROFILE_SUCCESS,
      user,
    }

  }

  function failure (error) {
    return {
      type: ACTIONS_TYPES.PROFILE_FAILURE,
      error,
    }

  }

}

