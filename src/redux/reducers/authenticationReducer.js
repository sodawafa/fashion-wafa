import { ACTIONS_TYPES } from '../constants'

let userCookie = JSON.parse(localStorage.getItem('user'))
let initialState = userCookie ? {
  loading: true,
  user: userCookie,
  email: null,
} : {}
initialState = {
  ...initialState,
  coupons: [
    {
      discount: '-10%',
      validity: 'Valide(s) dès 29,00€ d\'achat',
      coded: 'YGL1',
      date: '23/06/2021 07:30 ~ 24/06/2021 05:00'
    },
    {
      discount: '-15%',
      validity: 'Valide(s) dès 59,00€ d\'achat',
      coded: 'YGL2',
      date: '23/06/2021 07:30 ~ 24/06/2021 05:00'
    },
    {
      discount: '-20%',
      validity: 'Valide(s) dès 89,00€ d\'achat',
      coded: ' YGL3',
      date: '23/06/2021 07:30 ~ 24/06/2021 05:00'
    },
    {
      discount: '-5,00€',
      validity: 'Valide(s) dès 50,00€ d\'achat',
      coded: 'JFA1',
      date: '07/08/2019 04:00 ~ 31/08/2021 17:59'
    },
    {
      discount: '-10,00€',
      validity: 'Valide(s) dès 75,00€ d\'achat',
      coded: ' JFA2',
      date: '07/08/2019 04:00 ~ 31/08/2021 17:59'

    },
    {
      discount: '-15,00€',
      validity: 'Valide(s) dès 100,00€ d\'achat',
      coded: 'JFA0',
      date: '07/08/2019 05:11 ~ 31/08/2021 17:59'
    },
    {
      discount: '-30,00€',
      validity: 'Valide(s) dès 150,00€ d\'achat',
      coded: 'JFA3',
      date: '21/06/2021 04:55 ~ 27/06/2021 05:00'

    } ,
    {
      discount: '-50%',
      validity: 'Valide(s) dès 250,00€ d\'achat',
      coded: 'JFA4',
      date:'21/06/2021 05:11 ~ 27/06/2021 05:00'

    },   {
      discount: '-25%',
      validity: 'Valide(s) dès 75,00€ d\'achat',
      coded: 'DER1',
      date: '21/06/2021 05:11 ~ 27/06/2021 05:00'

    },


  ],
  coupons_expire: [],
}

export function authentication (state = initialState, action) {
  switch (action.type) {
    case ACTIONS_TYPES.LOGIN_REQUEST:
      return {
        loading: true,
        user: action.user,
      }
    case ACTIONS_TYPES.LOGIN_SUCCESS:
      return {
        loading: true,
        user: action.user,
      }
    case ACTIONS_TYPES.LOGIN_FAILURE:
      return {}
    case ACTIONS_TYPES.LOGOUT:
      return {}
    case ACTIONS_TYPES.USER_AUTHENTICATED:
      return {
        loading: true,
        user: action.user,
      }
    case ACTIONS_TYPES.PROFILE_REQUEST:
      return {
        loading: true,
        user: action.user,
      }
    case ACTIONS_TYPES.PROFILE_SUCCESS:
      return {
        loading: true,
        user: action.user,
      }
    case ACTIONS_TYPES.PROFILE_FAILURE:
      return {
        error: action.error,
      }
    case ACTIONS_TYPES.SIGNUP_REQUEST:
      return {
        loading: true,
        email: action.email,

      }
    case ACTIONS_TYPES.SIGNUP_SUCCESS:
      return {
        loading: true,
        email: action.email,
      }
    case ACTIONS_TYPES.SIGNUP_FAILURE:
      return {
        error: action.error,
      }
    case ACTIONS_TYPES.COUPON_ADD:
      return {
        ...state,

      }
    case ACTIONS_TYPES.COUPONS_GET:
      return {
        ...state,

      }
      case ACTIONS_TYPES.COUPON_EXPIRED_ADD:
      return {
        ...state,

      }
      case ACTIONS_TYPES.COUPON_EXPIRED_SET:
      return {
        ...state,

      }

    default:
      return state
  }

}
