import { combineReducers } from 'redux'
import { authentication } from './authenticationReducer'

const reducers = combineReducers({
  authentication,
})
export default reducers
