import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducers from './reducers'

function logger ({getState}) {
  return (next) => (action) => {
    return next(action)
  }

}
const store = createStore(
  reducers,
  applyMiddleware(
    thunkMiddleware,
    logger,
  )
)
export default store
