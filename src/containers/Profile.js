import React from 'react'
import { connect } from 'react-redux'
import '../styles/profile.css'
import { Link } from 'react-router-dom'
import icon from '../assets/gift-solid.svg'
import Footer from '../component/profile/Footer'
import plus from '../assets/icon-plus.png'
import Collapse from './Collapse'
import Carousel from './Carousel'
import Picture from '../component/Picture'
import Date from './Mydate'
import Mydate from './Mydate'

class Profile extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      fashions: this.props.fashions,

    }
    console.log(this.props.fashions)

  }


  render () {

    return (
      <div>
        <main className={'account'}>
          <section className={'page'}>
            <header className={'en-tete'}>
              <div>
                <p className={'text'}>Salut,

                  {
                    this.props.user && (
                      '  ' + this.props.user.firstName + '  ' +
                      this.props.user.lastName)
                  }
                </p>
                <ul className={'menu'}>
                  <li>
                    <span>6</span>
                    <br/><br/>
                    <Link to='coupon'>Coupons</Link>
                  </li>
                  <li>
                    <span>101</span>
                    <br/><br/>
                    <Link to="point">Points</Link>
                  </li>
                  <li>
                    <span>0</span>
                    <br/><br/>
                    <Link to='portefeuille'>Portefeuille</Link>
                  </li>
                  <li>
                    <img src={icon} alt="cadeau"/>
                    <Link to='cadeau'>Carte cadeau</Link>
                  </li>
                </ul>
              </div>
            </header>
            <nav className={'navigation'}>
              <div className={'nav-container'}>
                <h3 className={'nav-container-title'}>Mon Profile</h3>
                <div className={'content'}>
                  <Collapse title={'Mon Compte'}>
                    <div className={'collapse'}>
                      <ul className={'collapse-list'}>
                        <li><Link to="#">Mon Profile</Link></li>
                        <li><Link to="#">Mes Adresses</Link></li>
                        <li><Link to="#">Mes Options De Paiement</Link></li>
                        <li><Link to="#">Mes Mensurations</Link></li>
                        <li><Link to="#">Sécurité Du Compte</Link></li>
                      </ul>
                    </div>
                  </Collapse>

                </div>
                <div className={'content'}>
                  <Collapse title={'Mes Propriétés'}>
                    <div className={'collapse'}>
                      <ul className={'collapse-list'}>
                        <li><Link to="#">Mes Coupons</Link></li>
                        <li><Link to="#">Mes Points Bonus</Link></li>
                        <li><Link to="#">Mon Portefeuille</Link></li>
                        <li><Link to="#">Carte Cadeau</Link></li>
                      </ul>
                    </div>
                  </Collapse>

                </div>
                <div className={'content'}>
                  <Collapse title={'Mes Commandes'}>
                    <div className={'collapse'}>
                      <ul className={'collapse-list'}>
                        <li><Link to="#">Toutes Les Commandes </Link></li>
                        <li><Link to="#">Commandes Non Payées</Link></li>
                        <li><Link to="#">Commandes en Traitement</Link></li>
                        <li><Link to="#">Commandes Expédiées</Link></li>
                        <li><Link to="#">Commandes Contrôlées</Link></li>
                        <li><Link to="#">Commandes Renvoyées</Link></li>

                      </ul>
                    </div>
                  </Collapse>

                </div>
                <div className={'content'}>
                  <Collapse title={'Mon Historiques'}>
                    <div className={'collapse'}>
                      <ul className={'collapse-list'}>
                        <li><Link to="#">Liste D'Envies</Link></li>
                        <li><Link to="#">Récemment Vus</Link></li>
                      </ul>
                    </div>
                  </Collapse>

                </div>
                <div className={'content'}>
                  <Collapse title={'Services Clientèle'}>
                    <div className={'collapse'}>
                      <ul className={'collapse-list'}>
                        <li><Link to="#">Mes Notifications</Link></li>
                        <li><Link to="#">Archives Des Services</Link></li>
                      </ul>
                    </div>
                  </Collapse>

                </div>
                <div className={'content'}>
                  <Collapse title={'Autres Services'}>
                    <div className={'collapse'}>
                      <ul className={'collapse-list'}>
                        <li><Link to="#">Le Centre De L'Essai Gratuit</Link>
                        </li>
                        <li><Link to="#">Centre D'enquête</Link></li>
                        <li><Link to="#">Partagez & Gagnez</Link></li>
                      </ul>
                    </div>
                  </Collapse>

                </div>
                <div className={'content'}>
                  <Collapse title={'Politiques'}>
                    <div className={'collapse'}>
                      <ul className={'collapse-list'}>
                        <li><Link to="#">Livraison</Link></li>
                        <li><Link to="#">Politique De Retour</Link></li>
                        <li><Link to="#">Politique De Confidentialité <br/> &
                          Cookies</Link></li>
                      </ul>
                    </div>
                  </Collapse>

                </div>
                <div className={'content'}>
                  <span className={'menu-title'}>
                    <h4 className={'menu-content'}>Se Déconnecter</h4>
                  </span>
                  <div></div>

                </div>
              </div>

            </nav>
            <main className={'content'}>
              <div className={'content-menu'}>
                <div className={'content-body'}>
                  <div className={'content-body-title'}>
                    <span>Mes Commandes</span>
                    <span className={'content-body-link'}>
                      <Link to="#">VoirTout</Link>
                      <i className="fa fa-angle-right" aria-hidden="true"></i>
                    </span>

                  </div>
                  <ul className={'content-body-list'}>
                    <li className={'content-body-list-item'}>
                      <span className={'content-body-list-item-icon'}>
                      <i className="fa fa-credit-card" aria-hidden="true"></i>
                        </span>
                      <span className={'content-body-list-item-title'}>
                        Non payé
                      </span>

                    </li>
                    <li className={'content-body-list-item'}>
                      <span className={'content-body-list-item-icon-truck'}>
                        <i className="fa fa-truck  " aria-hidden="true"></i>

                        </span>
                      <span className={'content-body-list-item-title'}>
                       Expédiés
                      </span>

                    </li>
                    <li className={'content-body-list-item'}>
                      <span className={'content-body-list-item-icon'}>
                        <i className="fa fa-commenting" aria-hidden="true"></i>

                        </span>
                      <span className={'content-body-list-item-title'}>
                      Commentaire
                      </span>

                    </li>
                    <li className={'content-body-list-item'}>
                      <span className={'content-body-list-item-icon'}>
                        <i className="fa fa-reply" aria-hidden="true"></i>

                        </span>
                      <span className={'content-body-list-item-title'}>
                  Retour
                      </span>

                    </li>


                  </ul>
                  <ul className={'content-body-list-shopping'}>
                    <li className={'content-body-list-shopping-item'}>
                      <div
                        className={'content-body-list-shopping-date'}>
                        <Mydate/>

                      </div>
                      <div className={'carr'}>
                        {
                          this.state.fashions &&
                          this.state.fashions.filter((data) => data.id === 'a57ab8a7').length > 0 &&
                          <Carousel pictures={(this.state.fashions.filter((data) => data.id === 'a57ab8a7'))[0].pictures}/>
                        }
                        {/*{
                          this.state.fashions &&
                          this.state.fashions.map((json, index0) => {
                             if(json.pictures && json.pictures.length > 0)
                             return json.pictures.map((source, index) => {
                                return (
                                  <span key={index}>
                                    <img src={source} key={'pictures_' + index}/>
                                  </span>
                                )
                              })

                          })

                        }*/}

                      </div>

                    </li>

                  </ul>

                </div>


              </div>

            </main>

          </section>

        </main>
        <footer>
          <Footer/>
        </footer>
      </div>

    )
  }
}

const mapStateToProps = state => ({
  user: state.authentication.user,
})

const connectedLoginPage = connect(mapStateToProps)(Profile)
export { connectedLoginPage as Profile }

