import React from 'react'

class Mydate extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      date: new Date(),
    }
    this.timer = null
    this.tick = this.tick.bind(this)

  }

  componentDidMount () {
    this.timer = window.setInterval(this.tick, 1000)
  }

  componentWillMount () {
    window.clearInterval(this.timer)
  }

  tick () {
    this.setState({
      date: new Date(),
    })
  }

  render () {
    return (
      <div>
        {this.state.date.toLocaleDateString()} {this.state.date.toLocaleTimeString()}
      </div>
    )
  }
}

export default Mydate

