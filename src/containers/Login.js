import React, { Component } from 'react'
import { connect } from 'react-redux'
import { userAction } from '../redux/actions'
import user from '../assets/user-circle-solid.svg'
import '../styles/login.css'
import '../styles/signup.css'
import Footer from '../component/profile/Footer'

class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      submitted: true,
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    const user = this.props.dispatch(userAction.isAuthenticated())
    if (user) props.history.push('/user')
  }

  handleChange (e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  handleSubmit (e) {
    e.preventDefault()
    const { username, password } = this.state
    //const { username, password } = { username: 'tony@stark.com',password: 'password123' }
    const { dispatch } = this.props
    if (username && password) {
      dispatch(userAction.login(this.props, username, password))
    }
  }

  render () {
    const { loading } = this.props
    const { username, password, submitted } = this.state
    return (
      <div>
      <main className={'main bg-dark'}>
        <section className={'sign-in-content'}>
          <div className={'header'}>
            <h1>Sign In</h1>
            <img className={'icon'} src={user} alt="user"/>

          </div>
          <form method={"POST"} onSubmit={this.handleSubmit}>
            <div className={'wrapper'}>
              <label htmlFor="username">E-mail</label>
              <input type="text" id="username" name={'username'}
                     onChange={this.handleChange} placeholder={'xxxx@gmail.com'}/>
              {submitted && (username.length < 5) &&
              <div className={'required'}>Username is required</div>}

            </div>
            <div className={'wrapper'}>
              <label htmlFor="password">Password</label>
              <input type="password" id="password" name={'password'}
                     onChange={this.handleChange} placeholder={'Mot de passe'}/>
              {submitted && (password.length < 5) &&
              <div className={'required'}>Password is required</div>}
            </div>
            <button className={'btn-login'} type="submit">Sign In</button>
          </form>
        </section>
      </main>

      </div>

    )
  }
}

const mapStateToProps = state => ({
  loading: state.authentication.loading,
})

const connectedLoginPage = connect(mapStateToProps)(Login)
export { connectedLoginPage as Login }

