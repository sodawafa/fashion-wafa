import React from 'react'
import '../styles/carousel.css'

class Carousel extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      pictures: [],
      counter: 0,
      picture: null
    }
    this.next = this.next.bind(this)
    this.prev = this.prev.bind(this)
  }

  componentDidMount () {
    if(this.props.pictures && this.props.pictures.length > 0){
      console.log('done ',this.props.pictures)
      this.setState({
        pictures: this.props.pictures,
        picture: this.props.pictures[0]
      })
    }else {
      console.log('err',this.props.pictures)
    }
  }

  next (e) {
    let counter = this.state.counter + 1
    if (counter >= this.state.pictures.length) {
      counter = 0
    }
    this.setState({
      counter: counter,
      picture: this.state.pictures[counter],
    })
    e.preventDefault()
  }

  prev (e) {
    let counter = this.state.counter - 1
    if (counter < 0) {
      counter = this.state.pictures.length - 1
    }
    this.setState({
      counter: counter,
      picture: this.state.pictures[counter],
    })
    e.preventDefault()

  }

  nextBtn () {
    if (this.state.pictures.length > 1) {
      return <button className={'carousel_next'} onClick={this.next}><i
        className="fa fa-angle-right" aria-hidden="true"></i></button>
    }

  }

  prevBtn () {
    if (this.state.pictures.length > 1) {
      return <button className={'carousel_prev'} onClick={this.prev}><i
        className="fa fa-angle-left" aria-hidden="true"></i></button>
    }
  }

  render () {
    /*console.log(this.state.pictures)*/
    return (
      <div className={'content-body-list-shopping-carousel'}>
        <div className={'item-pic-1'}>
          <div className={'carousel-1'}
               >
            <img src={this.state.picture} style={{ width: 200, height: 200 , marginLeft: 56}}/>
            {this.nextBtn()}
            {this.prevBtn()}
          </div>
        </div>

      </div>
    )
  }
}

export default Carousel
