import React from 'react'
import { connect } from 'react-redux'
import { userAction} from '../redux/actions'

class Logout extends React.Component {

  constructor (props) {
    super(props)
    this.props.dispatch(userAction.logout(props))
  }

  render () {
    return (<div></div>)
  }
}

const connectedLogoutPage = connect()(Logout)
export { connectedLogoutPage as Logout }
