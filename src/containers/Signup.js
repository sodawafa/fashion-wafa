import React from 'react'
import { userAction } from '../redux/actions'
import user from '../assets/user-circle-solid.svg'
import { connect } from 'react-redux'
import { Login } from './Login'
import '../styles/signup.css'

class Signup extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      submitted: true,
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    //const user = this.props.dispatch(userAction.isAuthenticated())
    //if (user) props.history.push('/login')
  }

  handleChange (e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  handleSubmit (e) {
    e.preventDefault()
    const { email, password, firstName, lastName } = this.state
    const { dispatch } = this.props
    if (email && password && firstName && lastName) {
      dispatch(
        userAction.signup(this.props, email, password, firstName, lastName))
    }
  }

  render () {
    const { firstName, lastName, email, password, submitted } = this.state
    return (
      <main className={'main bg-dark'}>
        <section className={'sign-up-content'}>
          <div className={'header'}>
            <h1>Sign Up</h1>
            <img src={user} alt="user" className={'icon'}/>
          </div>
          <form onSubmit={this.handleSubmit}>
            <div className={'wrapper'}>
              <label htmlFor="firstName">Prénom</label>
              <input type="text" id="firstName" name={'firstName'}
                     placeholder={'Prénom'} onChange={this.handleChange}/>
              {submitted && (firstName.length < 5) &&
              <div className={'required'}>firstName is required</div>}
            </div>
            <div className={'wrapper'}>
              <label htmlFor="lastName">Nom</label>
              <input type="text" id="lastName" name={'lastName'}
                     placeholder={'Nom'} onChange={this.handleChange}/>
              {submitted && (lastName.length < 5) &&
              <div className={'required'}>lastName is required</div>}

            </div>
            <div className={'wrapper'}>
              <label htmlFor="email">E-mail</label>
              <input type="email" name={'email'} id={'email'}
                     placeholder={'xxxxxx@gmail.com'}
                     onChange={this.handleChange}/>
              {submitted && (email.length < 5) &&
              <div className={'required'}>E-mail is required</div>}

            </div>
            <div className={'wrapper'}>
              <label htmlFor="password">Mot de passe</label>
              <input type="password" name={'password'} id={'password'}
                     onChange={this.handleChange} placeholder={'Mot de passe '}/>
              {submitted && (password.length < 6) &&
              <div className={'required'}>Mot de passe is required</div>}

            </div>

            <button className={'btn-signup'} type="submit">Sign Up</button>

          </form>

        </section>

      </main>
    )
  }
}

const mapStateToProps = state => ({
  loading: state.authentication.loading,
})

const connectedLoginPage = connect(mapStateToProps)(Signup)
export { connectedLoginPage as Signup }
