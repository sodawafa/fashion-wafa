import React from 'react'
import '../styles/profile.css'

class Collapse extends React.Component {

  active = false

  constructor (props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      title: props.title,
      active: this.active,
    }

  }

  handleClick (e) {
    e.preventDefault()
    this.setState({
      active: !this.state.active,
    })
  }

  render () {
    return (
      <div className={`menu-title ${this.state.active ? 'active' : ''}`}>
        <h4 className={'menu-content'}>
          <div onClick={this.handleClick}>
            {this.state.title}
            <span className={"add-icon"}>
            <i className="fa fa-plus-square-o" aria-hidden="true"></i>
              </span>
            {/*<img src={plus} alt=""/>*/}
          </div>

        </h4>

        {this.state.active ? this.props.children :null}

      </div>

    )
  }
}

export default Collapse
